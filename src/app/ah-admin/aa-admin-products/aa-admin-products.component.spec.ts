import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AaAdminProductsComponent } from './aa-admin-products.component';

describe('AaAdminProductsComponent', () => {
  let component: AaAdminProductsComponent;
  let fixture: ComponentFixture<AaAdminProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AaAdminProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AaAdminProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
