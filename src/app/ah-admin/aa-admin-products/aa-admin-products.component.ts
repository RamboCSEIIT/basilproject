import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {AaFormSubmitService} from '../../aj-Services/ad-ProductForm/aa-form-submit.service';
import {Observable} from 'rxjs';
import {AngularFireList} from 'angularfire2/database';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-aa-admin-products',
  templateUrl: './aa-admin-products.component.html',
  styleUrls: ['./aa-admin-products.component.scss']
})
export class AaAdminProductsComponent implements OnInit {
  public products$: Observable<unknown[]>;

  public productss$: any[];
  filteredProducts;
  public term;


  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  searchKey: string;

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['courseName', 'fee', 'batch', 'subject', 'actions'];

  constructor(private  aaFormSubmitService: AaFormSubmitService) {

    this.aaFormSubmitService.getFullCourses().subscribe(
      list => {
        this.productss$ = list.map(item => {
          return {
            key: item.key,
            ...item.payload.val()
          };
        });
        this.listData = new MatTableDataSource(this.productss$);
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        /*
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele !== 'actions' && data[ele].toLowerCase().indexOf(filter) !== -1;
          });
        };

         */

      });
  }

  /*
  this.listData = new MatTableDataSource(array);
  this.listData.sort = this.sort;
  this.listData.paginator = this.paginator;
  this.listData.filterPredicate = (data, filter) => {
    return this.displayedColumns.some(ele => {
      return ele !== 'actions' && data[ele].toLowerCase().indexOf(filter) !== -1;
    });
  };
});

/*
this.products$ = this.aaFormSubmitService.getAll();

this.productss$ = ((this.products$) as any);
*/

  // console.log(this.productss$);


  ngOnInit(): void {
  }

  filter(query: string) {


    this.filteredProducts = (query) ?
      this.productss$.filter(p => p.courseName.toLowerCase().includes(query.toLowerCase())) :
      this.productss$;


  }

  onSearchClear() {
   this.searchKey = '';
   this.applyFilter();
  }

  applyFilter() {
  this.listData.filter = this.searchKey.trim().toLowerCase();
  }


}
