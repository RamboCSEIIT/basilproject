import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbAdminOrdersComponent } from './ab-admin-orders.component';

describe('AbAdminOrdersComponent', () => {
  let component: AbAdminOrdersComponent;
  let fixture: ComponentFixture<AbAdminOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbAdminOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbAdminOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
