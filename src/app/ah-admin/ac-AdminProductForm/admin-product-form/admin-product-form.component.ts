import {Component, OnInit, ViewChild} from '@angular/core';
import {AaSubjectService} from '../../../aj-Services/ac-CategorySubject/aa-subject.service';

// tslint:disable-next-line:import-spacing
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';

import {AbBatchService} from '../../../aj-Services/ac-CategorySubject/ab-batch.service';
import {AaFormSubmitService} from '../../../aj-Services/ad-ProductForm/aa-form-submit.service';
import {Config} from '../../../zz_Config/Config';


import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {ConfirmedValidator} from './confirmed.validator';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';

import 'rxjs/add/operator/take';
import {first, take} from 'rxjs/operators';

import * as firebase from 'firebase';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-admin-product-form',
  templateUrl: './admin-product-form.component.html',
  styleUrls: ['./admin-product-form.component.scss']
})
export class AdminProductFormComponent implements OnInit {
  subjects$: Observable<unknown[]>;

  public batches$: Observable<unknown>;

  id = null;

  model: any = {};

  constructor(private  router: Router, aaSubjectService: AaSubjectService, private route: ActivatedRoute,
              abBatchService: AbBatchService, private aaFormSubmitService: AaFormSubmitService) {


    this.subjects$ = aaSubjectService.getCourses();
    this.batches$ = abBatchService.getBatches();
    this.id = this.route.snapshot.paramMap.get('id');
    if (Config.doTest()) {
      console.log('yes id ' + this.id);
    }
    if (this.id) {
      this.model = this.aaFormSubmitService.get(this.id).valueChanges().take(1).subscribe(p => this.model = p);

    }
  }


  save(product) {
    if (Config.doTest()) {
      console.log(product);
    }

    if (this.id) {
      this.aaFormSubmitService.Update(this.id, product);
    }
    else
    {
      this.aaFormSubmitService.create(product);
    }

    this.router.navigate(['admin/products']);


  }

  Update(productId, product) {
    if (Config.doTest()) {
      console.log(product);
    }

    this.aaFormSubmitService.create(product);
    this.router.navigate(['admin/products']);


  }

  delete() {
    if (!confirm('Are you sure you want to delete this course?')) {
      return;
    }

    this.aaFormSubmitService.delete(this.id);
    this.router.navigate(['/admin/products']);

  }


  ngOnInit(): void {


  }

}
