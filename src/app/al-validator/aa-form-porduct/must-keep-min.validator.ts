
// custom validator to check that two fields match
import {FormGroup, Validators} from '@angular/forms';

export function MustMin(controlName: string) {
  const minValC = 5;

  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];


    // return null if controls haven't initialised yet
    if (!control ) {
      return null;
    }
    if (control.value < minValC)
    {
        control.setErrors({ minError: true });

    }
    else
    {
         control.setErrors({ MinError: false });

    }


  };
}
