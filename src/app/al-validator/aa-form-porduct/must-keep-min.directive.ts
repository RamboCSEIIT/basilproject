import {Directive, Input} from '@angular/core';
import {NG_VALIDATORS, Validator, AbstractControl, Validators, FormGroup, ValidationErrors} from '@angular/forms';
import {MustMatch} from './must-match.validator';
import {MustMin} from './must-keep-min.validator';



@Directive({
  selector: '[appMin]',
  providers: [{provide: NG_VALIDATORS, useExisting: MustKeepMinDirective, multi: true}]
})
export class MustKeepMinDirective implements Validator{

  constructor() { }
  @Input('appMin') min: number;
  registerOnValidatorChange(fn: () => void): void {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return Validators.min(this.min)(control);
  }


}




/*
  validate(control: AbstractControl): { [key: string]: any } {
    return Validators.min(this.min)(control);
}
  */
