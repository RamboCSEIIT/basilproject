export abstract class Config {
  public static myProp = 'Hello';
  public  static test = true;

  public static doSomething(): string {
    return 'World';
  }

  public static doTest(): boolean {
    return Config.test;
  }
}
