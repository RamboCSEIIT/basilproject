import { Component  } from '@angular/core';
import {AaAuthBasicService} from '../aj-Services/aa-authServices/aa-auth-basic.service';
import {AppUser} from '../ak-Models/aa-user.model';

@Component({
  selector: 'app-aa-navbar',
  templateUrl: './aa-navbar.component.html',
  styleUrls: ['./aa-navbar.component.scss']
})
export class AaNavbarComponent   {
  appUser: AppUser;
  constructor(public auth: AaAuthBasicService) {

    auth.appUser$.subscribe(appUser => this.appUser = appUser);

 }

 logout() {
    this.auth.logout();
  }
}
