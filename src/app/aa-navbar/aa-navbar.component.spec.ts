import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AaNavbarComponent } from './aa-navbar.component';

describe('AaNavbarComponent', () => {
  let component: AaNavbarComponent;
  let fixture: ComponentFixture<AaNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AaNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AaNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
