import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AeCheckoutComponent } from './ae-checkout.component';

describe('AeCheckoutComponent', () => {
  let component: AeCheckoutComponent;
  let fixture: ComponentFixture<AeCheckoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AeCheckoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AeCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
