import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {AaAuthBasicService} from './aj-Services/aa-authServices/aa-auth-basic.service';
import {Router} from '@angular/router';
import {AaUserBasicService} from './aj-Services/ab-UserServices/aa-user-basic.service';
import {Config} from './zz_Config/Config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'aaaBasilProject';
  FirstTime = true;

  constructor(private auth: AaAuthBasicService, router: Router, userService: AaUserBasicService) {

    auth.user$.subscribe(user => {

      if (!user) {
        return;
      }

      userService.save(user);

      if (Config.doTest()) {
        console.log('created app again');
      }

      const returnUrl = localStorage.getItem('returnUrl');

      if (!returnUrl) {
        return;

      }
      localStorage.removeItem('returnUrl');
      router.navigateByUrl(returnUrl);
      if (!this.FirstTime) {
        this.FirstTime = false;
      }


    });
  }

  ngOnInit() {
    const tag = document.createElement('script');

    tag.src = 'https://www.youtube.com/iframe_api';
    document.body.appendChild(tag);
  }
}
