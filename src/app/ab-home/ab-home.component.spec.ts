import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbHomeComponent } from './ab-home.component';

describe('AbHomeComponent', () => {
  let component: AbHomeComponent;
  let fixture: ComponentFixture<AbHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
