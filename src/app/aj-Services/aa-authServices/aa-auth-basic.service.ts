import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import {AngularFireAuth} from 'angularfire2/auth';
import {Observable, ObservableInput} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {AppUser} from '../../ak-Models/aa-user.model';
import {AaUserBasicService} from '../ab-UserServices/aa-user-basic.service';
import {map, switchMap} from 'rxjs/operators';
import 'rxjs/add/observable/of';
import {Config} from '../../zz_Config/Config';
@Injectable({
  providedIn: 'root'
})
export class AaAuthBasicService {
  user$: Observable<firebase.User>;

  constructor(private AfAuth: AngularFireAuth, private route: ActivatedRoute, private  userService: AaUserBasicService  ) {
    this.user$ = this.AfAuth.authState;
  }
  logout() {
    this.AfAuth.auth.signOut();
  }
  login() {
    const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
    if (Config.doTest()) {
      console.log('return url :: ' + returnUrl);
    }



    localStorage.setItem('returnUrl', returnUrl);

    this.AfAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
  }
  get appUser$(): Observable<AppUser> {
    const observable = this.user$.switchMap( user => {
      if (user)
      {
        return this.userService.get(user.uid).valueChanges();
      }
      else {
        return  Observable.of(null);
      }



 });
    return   observable;

  }

}
