import { TestBed } from '@angular/core/testing';

import { AaAuthBasicService } from './aa-auth-basic.service';

describe('AaAuthBasicService', () => {
  let service: AaAuthBasicService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AaAuthBasicService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
