import {Injectable} from '@angular/core';
import {AaAuthBasicService} from './aa-auth-basic.service';
import {CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AaUserBasicService} from '../ab-UserServices/aa-user-basic.service';
import 'rxjs/add/operator/switchMap';
import {Observable} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AcAdminAuthGuardService implements CanActivate {

  canActivate(): Observable<boolean>
  {

    return this.auth.appUser$.
      map (appUser => appUser.isAdmin);
  }

  constructor(private auth: AaAuthBasicService, private router: Router, private userBasicService: AaUserBasicService) {
  }


}

/*
this.auth.user$.switchMap(user => {

      return this.userBasicService.get(user.uid);
    }).subscribe(x => console.log(x));
 */
