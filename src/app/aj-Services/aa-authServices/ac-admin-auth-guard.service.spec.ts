import { TestBed } from '@angular/core/testing';

import { AcAdminAuthGuardService } from './ac-admin-auth-guard.service';

describe('AcAdminAuthGuardService', () => {
  let service: AcAdminAuthGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AcAdminAuthGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
