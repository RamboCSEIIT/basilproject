import { Injectable } from '@angular/core';
import {CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AaAuthBasicService} from './aa-auth-basic.service';

import 'rxjs/add/operator/map';

@Injectable( )
export class AbAuthGuardService  implements CanActivate
{
   constructor(private auth: AaAuthBasicService, private router: Router) { }

  canActivate( route, state: RouterStateSnapshot)
  {

     return this.auth.user$.map(user => {
       if (user) {
         return true;
       }
       console.log('Qparam from Auth redirect' + state.url);
       this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
       return  false;
     } );
  }
}
