import { TestBed } from '@angular/core/testing';

import { AbAuthGuardService } from './ab-auth-guard.service';

describe('AbAuthGuardService', () => {
  let service: AbAuthGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AbAuthGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
