import { TestBed } from '@angular/core/testing';

import { AbBatchService } from './ab-batch.service';

describe('AbBatchService', () => {
  let service: AbBatchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AbBatchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
