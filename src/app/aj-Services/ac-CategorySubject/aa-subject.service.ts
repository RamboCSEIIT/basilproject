import {Injectable} from '@angular/core';
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class AaSubjectService {
  itemsRef;
 constructor(private db: AngularFireDatabase) {
  }

  getSubjects() {
    this.itemsRef = this.db.list('/courses', ref => ref.orderByChild('name'));
    return  this.itemsRef.snapshotChanges();
  }
  getCourses() {

     return this.db.list('/courses', ref => ref.orderByChild('name')).valueChanges();
  }
}
