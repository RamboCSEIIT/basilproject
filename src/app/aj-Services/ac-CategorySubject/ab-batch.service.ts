import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {map} from 'rxjs/operators';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class AbBatchService {
  itemsRef;
  items;
  documentToDomainObject = _ => {

    const key = _.payload.key;
    const data = _.payload.val();
    data.key = key;
    return data;
    return ({key: _.payload.key, ..._.payload.val()});
  }

  constructor(private db: AngularFireDatabase) {
  }

  getBatchese() {
    return this.db.list('/batches', ref => ref.orderByChild('name')).valueChanges();

  }

  getBatches() {

    return this.db.list('/batches', ref => ref.orderByChild('name')).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
  }
}

/*
()
  //return this.db.list('/batches', ref => ref.orderByChild('name')).valueChanges();

 */
