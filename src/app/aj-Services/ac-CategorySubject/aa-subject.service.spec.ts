import { TestBed } from '@angular/core/testing';

import { AaSubjectService } from './aa-subject.service';

describe('AaSubjectService', () => {
  let service: AaSubjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AaSubjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
