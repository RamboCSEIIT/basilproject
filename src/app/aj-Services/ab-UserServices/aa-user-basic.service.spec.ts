import { TestBed } from '@angular/core/testing';

import { AaUserBasicService } from './aa-user-basic.service';

describe('AaUserBasicService', () => {
  let service: AaUserBasicService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AaUserBasicService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
