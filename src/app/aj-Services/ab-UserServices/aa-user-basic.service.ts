import { Injectable } from '@angular/core';

import { AngularFireDatabase, AngularFireObject, AngularFireList } from 'angularfire2/database';

import * as firebase from 'firebase';
import {AppUser} from '../../ak-Models/aa-user.model';





@Injectable({
  providedIn: 'root'
})
export class AaUserBasicService {

  constructor(private db: AngularFireDatabase) { }

  save(user: firebase.User) {
    this.db.object('/users/' + user.uid).update({
      name: user.displayName,
      email: user.email
    });
  }

  get(uid: string): AngularFireObject<AppUser>{
    return this.db.object('/users/' + uid);
  }
 }


/*

 */
