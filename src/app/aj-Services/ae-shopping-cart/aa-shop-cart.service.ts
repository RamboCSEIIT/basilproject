import {Injectable} from '@angular/core';
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';
import {take, map, filter} from 'rxjs/operators';
import {Product} from '../../ak-Models/ab-product.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AaShopCartService {
  itemsRef: AngularFireList<any>;
  items: Observable<Product[]>;
  product: Product;
  itemRef;

  constructor(private db: AngularFireDatabase) {
  }

  private create() {
    return this.db.list('/shopping-carts/').push(
      {
        dateCreated: new Date().getTime()
      }
    );
  }

  private getCartId(id: string) {
    return this.db.object('/shopping-carts/' + id);
  }

  public async getOrCreateCartId($product) {
    const cartId = localStorage.getItem('cartid');

    console.log(cartId);
    if (cartId) {
      return cartId;
    }


    const result = await this.create();


    localStorage.setItem('cartid', result.key);
    //// console.log('YESY' +  $product.fee);
    //  this.product.courseName = "lol";


    /*
    await  this.db.list('/shopping-carts/' + result.key + '/items').
    push($product);
    */


    return result.key;
  }

  async addToShoppingCart($product) {
    console.log($product);
    const cardId = await this.getOrCreateCartId($product);
    console.log('key' + $product);




    /*
    subscribe(action => {
      console.log(action.type);
      console.log(action.key);
      console.log(action.payload.val());
    });
  this.db.object('/shopping-carts/' + cardId + '/items/' + $product.key).
    update({quantity: 0}, (error) => {
      if (error) {
        // The write failed...
      } else {
        // Data saved successfully!
      }

    });
       usersRef.child(userId).once('value', function(snapshot) {
         var exists = (snapshot.val() !== null);
         userExistsCallback(userId, exists);
       });
{
      username: name,

    }, function(error) {
      if (error) {
        // The write failed...
      } else {
        // Data saved successfully!
      }
    }

   */
    // await  this.db.object('/shopping-carts/' + cardId + '/items/' + $product.key).update({ quantity: 0 }  );


    /*
        this.itemsRef.snapshotChanges().then(_ => console.log('success'))
          .catch(err => console.log(err, 'You dont have access!'));
    this.db.object('/shopping-carts/' + cardId + '/items/' + $product.key).snapshotChanges().then(action => {
          console.log(action.type);
          console.log(action.key);
          console.log(action.payload.val());
        }).catch(err => console.log(err, 'You dont have access!'));

        subscribe(c => {

          // tslint:disable-next-line:triple-equals
          if (c[0].quantity != undefined) {
            this.itemsRef.update($product.key, {product: $product, quantity: c[0].quantity + 1});
          } else {
            this.db.object('/shopping-carts/' + cardId + '/items/' + $product.key).update({quantity: 0, product: $product});

          }

        });
        */

  }
}
