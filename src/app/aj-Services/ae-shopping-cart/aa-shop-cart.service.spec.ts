import { TestBed } from '@angular/core/testing';

import { AaShopCartService } from './aa-shop-cart.service';

describe('AaShopCartService', () => {
  let service: AaShopCartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AaShopCartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
