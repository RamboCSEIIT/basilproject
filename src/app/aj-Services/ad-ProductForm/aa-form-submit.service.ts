import {Injectable} from '@angular/core';
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';
import * as firebase from 'firebase';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Config} from '../../zz_Config/Config';

@Injectable({
  providedIn: 'root'
})
export class AaFormSubmitService {

  minNum = 15;
  maxNum = 50;
  itemsRef: AngularFireList<any>;
  items: Observable<any[]>;

  constructor(private db: AngularFireDatabase) {
  }

  create(priduct) {
    return this.db.list('/products', ).push(priduct);
  }

  Update(productId, product) {

    return this.db.object('/products/' + productId, ).update(product);
  }


  delete(productId) {
    return this.db.object('/products/' + productId).remove();
  }

  getFullCourses() {
    this.itemsRef = this.db.list('/products', ref => ref.orderByChild('courseName'));
    return  this.itemsRef.snapshotChanges();
  }



  getAll() {

    this.itemsRef = this.db.list('/products', ref => ref.orderByChild('courseName'));
    // Use snapshotChanges().map() to store the key
    this.items = this.itemsRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({key: c.payload.key, ...c.payload.val()}))
      )
    );
    return this.items;

    /*
    snapshotChanges().pipe(map((mutation: any[]) => mutation.map(p => {
         return { ...p.payload.doc.data(), '$key': [p.payload.doc.id][0] };
         //return this.db.list('/products', ref => ref.orderByChild('name')).snapshotChanges();
     */
  }

  get(prodId) {
    return this.db.object('/products/' + prodId, );
  }

  getAlld() {

    this.itemsRef = this.db.list('/courses', ref => ref.orderByChild('name'));
    // Use snapshotChanges().map() to store the key
    this.items = this.itemsRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({key: c.payload.key, ...c.payload.val()}))
      )
    );
    return this.items;

    /*
    snapshotChanges().pipe(map((mutation: any[]) => mutation.map(p => {
         return { ...p.payload.doc.data(), '$key': [p.payload.doc.id][0] };
         //return this.db.list('/products', ref => ref.orderByChild('name')).snapshotChanges();
     */
  }
}
