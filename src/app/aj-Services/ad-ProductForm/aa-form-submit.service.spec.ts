import { TestBed } from '@angular/core/testing';

import { AaFormSubmitService } from './aa-form-submit.service';

describe('AaFormSubmitService', () => {
  let service: AaFormSubmitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AaFormSubmitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
