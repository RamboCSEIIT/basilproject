import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfOrderSuccessComponent } from './af-order-success.component';

describe('AfOrderSuccessComponent', () => {
  let component: AfOrderSuccessComponent;
  let fixture: ComponentFixture<AfOrderSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfOrderSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfOrderSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
