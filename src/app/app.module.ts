import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {environment} from '../environments/environment';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AiLoginComponent } from './ai-login/ai-login.component';
import {AaNavbarComponent} from './aa-navbar/aa-navbar.component';
import { AbHomeComponent } from './ab-home/ab-home.component';
import { AcProductsComponent } from './ac-products/ac-products.component';
import { AdShoppingCartComponent } from './ad-shopping-cart/ad-shopping-cart.component';
import { AeCheckoutComponent } from './ae-checkout/ae-checkout.component';
import { AfOrderSuccessComponent } from './af-order-success/af-order-success.component';
import { AgMyOrdersComponent } from './ag-my-orders/ag-my-orders.component';

import { AaAdminProductsComponent } from './ah-admin/aa-admin-products/aa-admin-products.component';
import { AbAdminOrdersComponent } from './ah-admin/ab-admin-orders/ab-admin-orders.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { RouterModule } from '@angular/router';
import {AaAuthBasicService} from './aj-Services/aa-authServices/aa-auth-basic.service';
import {AbAuthGuardService} from './aj-Services/aa-authServices/ab-auth-guard.service';
import {AaUserBasicService} from './aj-Services/ab-UserServices/aa-user-basic.service';
import {AcAdminAuthGuardService} from './aj-Services/aa-authServices/ac-admin-auth-guard.service';
import { AdminProductFormComponent } from './ah-admin/ac-AdminProductForm/admin-product-form/admin-product-form.component';

import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import {MustMatchDirective} from './al-validator/aa-form-porduct/must-match-directive.directive';
import { MustKeepMinDirective } from './al-validator/aa-form-porduct/must-keep-min.directive';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import { AaProductFilterComponent } from './ac-products/aa-filter/aa-product-filter/aa-product-filter.component';
import { AaProductCardComponent } from './ac-products/ab-card/aa-product-card/aa-product-card.component';
import {AaShopCartService} from './aj-Services/ae-shopping-cart/aa-shop-cart.service';

import { SafePipe } from './za-pipe/aa-sanitizer/safe.pipe';
import { SampleComponent } from './am-Grid/sample/sample.component';

@NgModule({
  declarations: [
    AppComponent,

    AaNavbarComponent,

    AbHomeComponent,

    AcProductsComponent,

    AdShoppingCartComponent,

    AeCheckoutComponent,

    AfOrderSuccessComponent,

    AgMyOrdersComponent,

    AaAdminProductsComponent,

    AbAdminOrdersComponent,

    AiLoginComponent,

    AdminProductFormComponent,

    MustMatchDirective,

    MustKeepMinDirective,

    AaProductFilterComponent,

    AaProductCardComponent,
    SafePipe,
    SampleComponent,


  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    NgbModule,
    YouTubePlayerModule,
    Ng2SearchPipeModule,
    MatTableModule,
    MatIconModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    RouterModule.forRoot([
   //   {path: '', component: AcProductsComponent},
      {path: '', component: SampleComponent},

      {path: 'products', component: AcProductsComponent},
      {path: 'shopping-cart', component: AdShoppingCartComponent},
      {path: 'login', component: AiLoginComponent},

      {path: 'check-out', component: AeCheckoutComponent, canActivate: [AbAuthGuardService]},
      {path: 'order-success', component: AfOrderSuccessComponent, canActivate: [AbAuthGuardService]},
      {path: 'my/orders', component: AgMyOrdersComponent, canActivate: [AbAuthGuardService]},
      {
        path: 'admin/products/new',
        component: AdminProductFormComponent,
        canActivate: [AbAuthGuardService, AcAdminAuthGuardService]
      },
      {
        path: 'admin/products/:id',
        component: AdminProductFormComponent,
        canActivate: [AbAuthGuardService, AcAdminAuthGuardService]
      },
      {
        path: 'admin/products',
        component: AaAdminProductsComponent,
        canActivate: [AbAuthGuardService, AcAdminAuthGuardService]
      },
      {
        path: 'admin/orders',
        component: AbAdminOrdersComponent,
        canActivate: [AbAuthGuardService,
          AcAdminAuthGuardService]
      },

    ]),
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatCardModule
  ],

  providers: [AaAuthBasicService, AbAuthGuardService, AaUserBasicService, AcAdminAuthGuardService,  AaShopCartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
