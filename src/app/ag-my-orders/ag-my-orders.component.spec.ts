import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgMyOrdersComponent } from './ag-my-orders.component';

describe('AgMyOrdersComponent', () => {
  let component: AgMyOrdersComponent;
  let fixture: ComponentFixture<AgMyOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgMyOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgMyOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
