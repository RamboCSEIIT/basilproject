import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdShoppingCartComponent } from './ad-shopping-cart.component';

describe('AdShoppingCartComponent', () => {
  let component: AdShoppingCartComponent;
  let fixture: ComponentFixture<AdShoppingCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdShoppingCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdShoppingCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
