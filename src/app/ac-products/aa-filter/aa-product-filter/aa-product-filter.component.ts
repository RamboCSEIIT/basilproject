import {Component, Input, OnInit} from '@angular/core';
import {AaSubjectService} from '../../../aj-Services/ac-CategorySubject/aa-subject.service';

@Component({
  selector: 'app-aa-product-filter',
  templateUrl: './aa-product-filter.component.html',
  styleUrls: ['./aa-product-filter.component.scss']
})
export class AaProductFilterComponent implements OnInit {
  $subjects;

 @Input() subject: string;
  constructor(public aaSubjectService: AaSubjectService) {


    aaSubjectService.getSubjects().subscribe(
      list => {
        this.$subjects = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });


      });

  }

  ngOnInit(): void {
  }

}
