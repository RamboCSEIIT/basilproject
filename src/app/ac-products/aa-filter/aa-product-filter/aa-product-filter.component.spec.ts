import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AaProductFilterComponent } from './aa-product-filter.component';

describe('AaProductFilterComponent', () => {
  let component: AaProductFilterComponent;
  let fixture: ComponentFixture<AaProductFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AaProductFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AaProductFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
