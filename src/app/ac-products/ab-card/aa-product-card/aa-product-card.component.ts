import {Component, Input, OnInit} from '@angular/core';
import {AaShopCartService} from '../../../aj-Services/ae-shopping-cart/aa-shop-cart.service';
import {$} from 'protractor';

@Component({
  selector: 'app-aa-product-card',
  templateUrl: './aa-product-card.component.html',
  styleUrls: ['./aa-product-card.component.scss']
})
export class AaProductCardComponent implements OnInit {
  @Input() $product;


  constructor(private shopcart: AaShopCartService) {
  }

  ngOnInit(): void {
  }

  deletevideo(id) {


    const listaFrames = document.getElementsByTagName('iframe');

    for (let i = 0; i < listaFrames.length; i++) {
      console.log ('Block statement execution no.' + i);

      const iframe = listaFrames[i].contentWindow;
      iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');

    }



    // document.getElementById(id).pla

    /*
    console.log(id);
    document.getElementById(id).get  contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
*/

  }

  callFunction(event, post) {
    console.log(post);
  }

  addToCart($product) {

    this.shopcart.addToShoppingCart($product);

  }
}
