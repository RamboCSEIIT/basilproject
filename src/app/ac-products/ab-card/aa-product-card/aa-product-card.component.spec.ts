import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AaProductCardComponent } from './aa-product-card.component';

describe('AaProductCardComponent', () => {
  let component: AaProductCardComponent;
  let fixture: ComponentFixture<AaProductCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AaProductCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AaProductCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
