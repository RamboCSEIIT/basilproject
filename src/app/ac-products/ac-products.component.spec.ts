import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcProductsComponent } from './ac-products.component';

describe('AcProductsComponent', () => {
  let component: AcProductsComponent;
  let fixture: ComponentFixture<AcProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
