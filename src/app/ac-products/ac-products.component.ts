import {Component, OnInit} from '@angular/core';
import {AaAuthBasicService} from '../aj-Services/aa-authServices/aa-auth-basic.service';
import {AaFormSubmitService} from '../aj-Services/ad-ProductForm/aa-form-submit.service';
import {AaSubjectService} from '../aj-Services/ac-CategorySubject/aa-subject.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-ac-products',
  templateUrl: './ac-products.component.html',
  styleUrls: ['./ac-products.component.scss']
})
export class AcProductsComponent implements OnInit {


  $products;
  $filterProducts;

  subject: string;

  constructor(route: ActivatedRoute, public aaFormSubmitService: AaFormSubmitService) {

    aaFormSubmitService.getFullCourses().subscribe(
      list => {
        this.$products = list.map(item => {
          return {
            key: item.key,
            ...item.payload.val()
          };
        });

        route.queryParamMap.subscribe(params => {



          this.subject = params.get('subject');
          console.log(this.subject);
          this.$filterProducts = (this.subject) ? this.$products.filter(p => p.subject === this.subject) : this.$products;


          // this.$filterProducts = (this.subject) ? this.$products : this.$products;

        });
      });

/*
    route.paramMap.subscribe(params => {
      this.subject = params.get('subject');
    //  this.$filterProducts = (this.subject) ? this.$products.filter(p => p.$key === this.subject) : this.$products;
      this.$filterProducts = (this.subject) ? this.$products : this.$products;

    });*/
  }


  ngOnInit(): void {
  }

}
