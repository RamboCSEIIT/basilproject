export class Product {

  public batch: string;
  public courseName: string;
  public fee: number;
  public imageVideo: string;
  public subject: string;
}
