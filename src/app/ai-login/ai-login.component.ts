import { Component  } from '@angular/core';

import {AaAuthBasicService} from '../aj-Services/aa-authServices/aa-auth-basic.service';
@Component({
  selector: 'app-ai-login',
  templateUrl: './ai-login.component.html',
  styleUrls: ['./ai-login.component.scss']
})
export class AiLoginComponent   {

  constructor(private auth: AaAuthBasicService) { }

 login() {
   this.auth.login();
  }

}
