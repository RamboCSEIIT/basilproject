import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AiLoginComponent } from './ai-login.component';

describe('AiLoginComponent', () => {
  let component: AiLoginComponent;
  let fixture: ComponentFixture<AiLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AiLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AiLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
