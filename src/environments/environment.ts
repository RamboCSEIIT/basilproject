// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: 'AIzaSyBi16Wj8rJdo4K2pwqxpKWUYGX7MWHNVbU',
    authDomain: 'basilproject-27c94.firebaseapp.com',
    databaseURL: 'https://basilproject-27c94.firebaseio.com',
    projectId: 'basilproject-27c94',
    storageBucket: 'basilproject-27c94.appspot.com',
    messagingSenderId: '845191902306',
    appId: '1:845191902306:web:b030aaa90e1ee9f3150d3b',
    measurementId: 'G-7BX6PYRWJX'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
